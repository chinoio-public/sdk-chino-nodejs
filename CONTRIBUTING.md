# Contributing

If you have some suggestions, improvements or bug fixes, please follow these steps:

- fork the repository. Remember to append your name to the list of contributors
  in the [authors file](./AUTHORS.md)
  
- install the dev dependencies with

        npm install
        npm install --only=dev
  
- develop. If you are adding new logic, **write tests** for it.
  
- launch tests for your platform:
  
  > Windows

        npm run test_windows
  
  > Linux

        npm run test_bash

    :warning: **be careful!**:
  
    > the tests will **delete EVERYTHING stored in the account** in order to preserve the correctness of the tests. Run them on a custom account and always against the test API `https://api.test.chino.io`.

- create a Merge Request to the original repository.