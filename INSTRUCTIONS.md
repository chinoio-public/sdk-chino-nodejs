# [COMMUNITY] Chino Node.js SDK <!-- omit in toc -->
[![Build Status](https://travis-ci.org/chinoio/chino-nodejs.svg?branch=master)](https://travis-ci.org/chinoio/chino-nodejs)

Node.js SDK for Chino.io API

**Community SDK:** Most of the content and the code has been provided 
by third-parties.

- - -
## Table of Contents <!-- omit in toc -->
- [Requirements](#requirements)
- [Installation](#installation)
- [Docs generation](#docs-generation)
- [Calling the API](#calling-the-api)
- [First steps](#first-steps)
  - [Authenticate as Admin](#authenticate-as-admin)
  - [Authenticate as User](#authenticate-as-user)
  - [API calls](#api-calls)
    - [A note about BLOBs](#a-note-about-blobs)
- [Test the SDK](#test-the-sdk)

- - -

## Requirements
Before you can use this SDK, you have to install Node JS Javascript runtime. If you haven't it yet, you can 
follow the [instructions](https://nodejs.org/en/download/package-manager/) provided on Node JS website.

You may also find useful the `npm` package manager.

## Installation
To install Chino SDK in your Node JS project you can run the following command

    npm install --save chinoio
    
The above command will download Chino SDK in your `node_modules` directory and will add the dependency in your 
`package.json file`.

## Docs generation
Generate the SDK docs locally with:

    npm run generate_docs

Then open `docs/chinoio/2.0.0/index.html` in your browser.

## Calling the API

Since requesting to REST service requires some waiting time, each call is  
asynchronous: each function of the SDK returns a 
[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) 
that will be resolved with the `then` handler if the server responds with a 
status code of `200`, otherwise it will be rejected and will execute 
the `catch` handler.

If you have to make sequential calls, you have to chain returned 
Promises from each call:

```javascript
doSomething(data).then((result) => {
        // handle 'result'
        doSomethingElse(otherData).then((finalResult) => {
                // handle 'finalResult' and 'result'
            })
            .catch((error) => {
                // handle errors of doSomethingElse()
            });
    })
    .catch((error) => {
        // handle errors of doSomething()
    });
```

## First steps
Import the SDK in your project:

```javascript
const Chino = require("chino-sdk");
```

Now you will need an API client to perform API calls. 
There are 2 types of clients for different purposes:

- [Customer client](#authenticate-as-admin): 
  no access restrictions, only for admin purposes

- [User client](#authenticate-as-user):
  access is regulated by granular Permissions

### Authenticate as Admin

```javascript
const chinoClient = new Chino("base_url", "customer_id", "customer_key");
```

This client allows to change everything in the Chino.io account.

**Only use this auth method in back-end code.**

- `base_url`:

    the URL of the Chino.io API, one of:
        
    - https://api.test.chino.io/v1 for the **test** API
    - https://api.chino.io/v1 for the **production** API

    **NOTE:** do NOT append a slash `/` at end of the URL.

- `customer_id`:
  
  your Customer ID on the Chino.io platform

- `customer_key`:
  
  one of the Customer Keys associated with your account

### Authenticate as User

```javascript
const userClient = new Chino("base-url", "access_token");
```

- `base_url`:

    the URL of the Chino.io API, one of:
        
    - https://api.test.chino.io/v1 for the **test** API
    - https://api.chino.io/v1 for the **production** API

    **NOTE:** do NOT append a slash `/` at end of the URL.

- `access_token`:
  
  an OAuth2 Bearer Token provided by the user. 
  
  To learn how to get the `access_token`, continue reading below.
  Otherwise, you can just [skip to the next section](#api-calls).

#### User login <!-- omit in toc -->

This client will be limited by user Chino permissions 
(see docs for further information on permissions).
 
In order to send API requests to Chino.io, you have to:

1. Register your *Users* on Chino.io

2. Create an *Application* on Chino.io and choose the correct `client_type`:

    - `public` clients, i.e. a mobile or desktop apps.
    - `confidential` clients, i.e. applications that run on a server
 
    ```javascript
    const appData = {
        grant_type: "password",
        name: "Application test",
        client_type: "confidential"
    }

    chinoClient.applications.create(appData)
        .then((app) => {
            // enable chino client to authenticate application users 
            chinoClient.setAuth(app.app_id, app.app_secret);
        })
        .catch((error) => {
            // manage request error
            console.log(error);
        });
    ```

    This call returns an `application_id` and an `application_secret` that 
    you need to store somewhere.

    **NOTE:** These properties must be kept private for security reasons, but
    `public` clients allow to authenticate a User without `application_secret`
    in order to avoid storing sensitive data on a vulnerable device.

3. Authenticate a *User* with his own `username` and `password`: 
   you will get an `access_token`, use it to create an API client 
   for the *User*.
   
   **NOTE**: the access token has an expiration, see docs for managing it.
    
    ```javascript
    let userClient = null;
     
    chinoClient.auth.login(username, password)
        .then((auth) => {
            userClient = new Chino("base-url", auth.access_token);
        })
    ```

To learn more about the authentication types Chino.io provides, check out our
[tutorial](https://console.test.chino.io/docs/tutorials/tutorial-auth) 
on the topic, or 
[read the full docs](https://console.test.chino.io/docs/v1#user-authentication)
about it.

    
### API calls

Now we have a client object. Let's us make a call to Chino APIs.

For example, let's create a *Repository* on Chino, sending an object (JSON)
with the correct structure, as defined in the 
[Chino API docs](https://console.test.chino.io/docs/v1#repositories-repositories-post):

```javascript
const data = {
    description: "This is a test repository"
}

chinoClient.repositories.create(data)
    .then((repository) => {
        console.log(repository.description)
    }).catch((err) => {
        console.log("!! error while creating a Repository:" + "\n" + err);
    });
```

#### A note about BLOBs
Through the SDK you can uploaed **BLOBs**, i.e. binary files that
are divided in chunks to be uploaded in parallel.

Since the server limits the number of concurrent calls a user can do, 
you should set the `chunkSize` variable wisely in order to not exceed 
**~10 parallel calls**. 

The default chunk size is of `100MB`, which works for most of the cases.

## Test the SDK
1. Clone this repository on your local machine:
    
        git clone https://gitlab.com/chinoio-public/sdk-chino-nodejs.git

2. `cd` inside repository folder:
   
        cd chino-nodejs

3. Install project dependencies and dev dependencies:

        npm install
        npm install --only=dev
    
4. Set the required environment variables. 
   You need to have an account on the Chino.io platform.

    > Linux

        export url="https://api.test.chino.io/v1"
        export customer_id="your-Chino-Customer-ID"
        export customer_key="your-Chino-Customer-KEY"
        
    > Windows (cmd) - NOTE: do not put quotes "" after the `=`

        set url=https://api.test.chino.io/v1
        set customer_id=your-Chino-Customer-ID
        set customer_key=your-Chino-Customer-KEY
        
    > Windows (powershell)

        $env:url="https://api.test.chino.io/v1"
        $env:customer_id="your-Chino-Customer-ID"
        $env:customer_key="your-Chino-Customer-KEY"
    
5. Run the tests:

    > Linux

        npm run test_bash
        
    > Windows

        npm run test_windows
