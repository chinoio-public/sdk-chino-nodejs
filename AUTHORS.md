# SDK Contributors

- Daniele Bissoli (dani95bissoli@gmail.com)
- Andrea Arighi (andrea@chino.io), Chino.io
- Stefano Tranquillini (stefano@chino.io), Chino.io

- - -

*Add your info on the bottom of the list. Suggested format:*

    - name, (email), company
