# [COMMUNITY] Chino Node.js SDK <!-- omit in toc -->

[![Build Status](https://travis-ci.org/chinoio/chino-nodejs.svg?branch=master)](https://travis-ci.org/chinoio/chino-nodejs)

Node.js SDK for Chino.io API

**Community SDK:** Most of the content and the code has been provided 
by third-parties.

**Version**: `2.0.0`
 
**Useful links**
 - [Full SDK instructions](./INSTRUCTIONS.md)
 - [Generate SDK docs](./INSTRUCTIONS.md#docs-generation)
 - [Chino.io API docs](http://console.test.chino.io/docs/v1)

For issues or questions, please contact [tech-support@chino.io](mailto:tech-support@chino.io).

-------------------------------------------------------------------------------
#### Table of content <!-- omit in toc -->
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage example](#usage-example)

-------------------------------------------------------------------------------

## Requirements

- Node.js with NPM package manager - [Download for your system](https://nodejs.org)


## Installation

    npm install --save chinoio

## Usage example
The most common task you can perform is the creation of a Document, where data can be stored.

*If you want to learn more about how the SDK works, check the [full instructions](./INSTRUCTIONS.md).*

### Hello, World! <!-- omit in toc -->

1. Import the SDK in your project:

    ```javascript
    const Chino = require("chino-sdk");
    ```

2. Create an instance of the API client

    ```javascript
    let customerId = "<your-customer-id>"
    let customerKey = "<your-customer-key>"
    let server = "https://api.test.chino.io/v1"
    const chinoClient = new Chino(server, customer_id, customer_key);
    ```

3. Define a *Repository*:

    ```javascript
    let data_repository = {
        "description": "my awesome Repository"
    };
    ```

4. Define the *Schema* of your *Document*:
        
    ```javascript
    let data_schema = {
        "description": "my astounding Schema",
        "structure": {
            "fields": [
                {
                    "name": "greeting",
                    "type": "string",
                    "indexed": true
                }
            ]
        }
    }
    ```

5. Define the *Document*:

    ```javascript
    let data_document = {
        "greeting": "Hello, World!"
    }
    ```

6. Create the objects with the SDK:

    ```javascript
    // Create the Repository (3.)
    chinoClient.repositories.create(data_repository).then((repository) => {
        let repo_id = repository.repository_id;

        // Create Schema (4.) inside the Repository
        chinoClient.schemas.create(repo_id, data_schema).then((schema) => {
            let schema_id = schema.schema_id;

            // Create Document (5.) inside the Schema
            chinoClient.documents.create(schema_id, data_document).then((document) => {
                let doc_id = document.document_id;

                console.log("SUCCESS! Created Document " + doc_id);
            }).catch((error) => {
                let msg = "Failed to create Document.";
                console.console.log(msg + "\n" + error);
            });
            
        }).catch((error) => {
            let msg = "Failed to create Schema.";
            console.console.log(msg + "\n" + error);
        });

    }).catch((error) => {
        let msg = "Failed to create Repository.";
        console.console.log(msg + "\n" + error);
    });
    ```
