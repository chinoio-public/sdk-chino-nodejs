@rem Prints the correct instructions to test the SDK on Windows.
@rem Works on both Powershell and cmd.

@ECHO off

@rem show message with the correct test commands
echo.
echo - - - - - - - - - - - - - - - - - - - -
echo  On Windows run: 'npm run test_windows'
echo - - - - - - - - - - - - - - - - - - - -
echo.

@rem make `npm test` fail
EXIT /B 1

@ECHO on